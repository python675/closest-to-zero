import unittest


from Application import Application


class TestApplication(unittest.TestCase):
    def setUp(self):
        self.application = Application()


class TestClosestToZero(TestApplication):
    def test_closest_to_zero(self):
        expect = 1
        input_list = [4, 9, 1, 7]
        output = self.application.select_closest_to_zero(input_list)
        self.assertEqual(expect, output)