# Goal for this project
1. First time to devlop **Python project** with TDD. 
2. First time to write unittest on **Python project**.
***
## Closest to zero
```
Given a list of integers find the closest to zero.
If there is a tie, choose the positive value.
```

### Thank you : [cyber-dojo](https://cyber-dojo.org/creator/choose_problem "cyber-dojo: Closest to zero") 